package com.caliso.agent;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.TestRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import com.caliso.utils.ManageVar;
import com.caliso.utils.ReportData;
import com.caliso.utils.ScreenshotListener;

import java.lang.Math;

public class LyftAppiumAgent {
    
    private static AppiumDriver driver;
    private static boolean previousFailed = false;
    //private static String myAddress;
    static private  Logger log = LogManager.getLogger("AppiumAgent");
    static private String port = "4724";
    
  //IDs
    String loginID = "me.lyft.android:id/facebook_login_button";
    String requestLyftID = "me.lyft.android:id/request_ride_button";
    String cancelPopUpID = "me.lyft.android:id/cancel_button";
    String addressMainActivityID = "me.lyft.android:id/pickup_address_txt";
    String querryBackIconID = "me.lyft.android:id/back_button";
    String clearID = "me.lyft.android:id/clear_button";
    String querryFieldID = "me.lyft.android:id/query_edit_text";
    String ballonQuerryResultID = "me.lyft.android:id/place_icon_image_view";
    String querryResultsID = "me.lyft.android:id/places_listview";
    String querryAddress = "me.lyft.android:id/place_subtitle_text_view";
    String etaMSgID = "me.lyft.android:id/ride_hint_txt";
    
    String primeTimeID = "me.lyft.android:id/prime_time_info_button";
    String HappyHourID = "me.lyft.android:id/happy_hour_info_button";
    String primeTimeMsgID ="me.lyft.android:id/dialog_title_text_view";
    String gotItButtonID = "me.lyft.android:id/dialog_positive_button";
    
    String lyftIconXpath = "//android.widget.TextView[contains(@text,'Lyft')]";
	String apkFileName="Lyft-me.lyft.android-72-v1.4.7.apk";

    @DataProvider(name = "DP1")
    public Object[][] createData() {
        String [][] standardAddresses = new String [][]{
                {
                    "2127 24th St, San Francisco",
                    "37.753342", 
                    "-122.401074"
                  },
                  {
                    "480 Geary St, San Francisco",
                    "37.787428", 
                    "-122.411130"
                  },
                  {
                    "2794 California St, San Francisco",
                    "37.788308",
                    "-122.440283"
                  },
                  {
                    "301 Van Ness Ave, San Francisco",
                    "37.778678", 
                    "-122.420370"
                  },
                  {
                    "383 Garces Dr, San Francisco",
                    "37.716778", 
                    "-122.483182"
                  },
                  {
                    "911 Paris St, San Francisco",
                    "37.715697", 
                    "-122.439576"
                  },
                  {
                    "250 Teddy Ave, San Francisco",
                    "37.715348", 
                    "-122.407476"
                  },
                  {
                    "1001 Portola Dr, San Francisco",
                    "37.742126", 
                    "-122.455563"
                  },
                  {
                    "2299 Sunset Blvd, San Francisco",
                    "37.744349", 
                    "-122.494507"
                  },
                  {
                    "910 Church St, San Francisco",
                    "37.756487", 
                    "-122.428014"
                  },
                  {
                    "74 Carmel St, San Francisco",
                    "37.761274", 
                    "-122.448437"
                  },
                  {
                    "3601 Balboa St, San Francisco",
                    "37.775698", 
                    "-122.497556"
                  },
                  {
                    "1019 Clement St, San Francisco",
                    "37.784568", 
                    "-122.469987"
                  },
                  {
                    "1599 Lane St, San Francisco",
                    "37.732529", 
                    "-122.389488"
                  },
                  {
                    "1635 35th Ave, San Francisco",
                    "37.756765", 
                    "-122.494111"
                  },
                  {
                      "San Francisco International Airport",
                      "37.621559",
                      "-122.378950"
                  }
                 };
        return(standardAddresses);
    }

  
  @BeforeClass
  public void setup(ITestContext c) throws Exception
  {
      try
      {
          //Get variables
          port = ManageVar.getVarString("canoeport", port);
          //Appium setup
          launchApp();
          login();
          TestRunner tr = (TestRunner) c;
          tr.addTestListener(new ScreenshotListener(driver));
      }
      catch (Exception e)
      {
          Reporter.log("Error: "+ e.getMessage(),true);
          driver.quit();
          throw e;
      }
  }
  
  @AfterClass
  public void tearDown()
  {
      Reporter.log("Press the Home Button",true);
      driver.sendKeyEvent(3);      
      driver.quit();
  }
  @BeforeMethod
  public void setupBeforeTest() throws MalformedURLException
  {
      if(previousFailed)
      {
          //SendMail.email("Fatal Error", "An error has occured go check automation", "stathism@gmail.com,stathis@anfacto.com");
          launchApp();
          login();
      }
      
      previousFailed = true;
  }
  
  @Test(dataProvider = "DP1")
  public void getData(String myAddress,String lat, String lon) throws InterruptedException, IOException, ParseException 
  {   
      waitForMainPage();
      Reporter.log("Call setAddress with address:"+myAddress,true);
      setAddress(myAddress);
      getPriceAndEtaAndReport(myAddress,lat,lon);
      Reporter.log("End Test",true);
      previousFailed = false;
  }
  private void getPriceAndEtaAndReport(String address,String lat, String lon) throws InterruptedException, IOException, ParseException
  {
      int surge = 100;
      waitForMainPage();
      Reporter.log("Verify that the address has not changed",true);
      Reporter.log("Instantiate address field with ID:"+addressMainActivityID,true);
      WebElement addressMainActivity =driver.findElement(By.id(addressMainActivityID));
      Reporter.log("Current address:"+addressMainActivity.getText(),true);
      
      if(!addressMainActivity.getText().contains(address.split("\\s+")[1]))
      {
          setAddress(address);
      }
      Reporter.log("Instantiate the ETA string id:"+etaMSgID,true);
      WebElement etaMsg = driver.findElement(By.id(etaMSgID));
      Reporter.log("Msg is :"+etaMsg.getText(),true);
      String eta = extractETA(etaMsg.getText());
      log.info("ETA:"+eta);
      if(!eta.equals("-1"))
      {
          Reporter.log("Get all Prime Time buttons ID:"+primeTimeID,true);
          List<WebElement> primeTimeArray = driver.findElements(By.id(primeTimeID));
          Reporter.log("Number of Prime Time buttons:"+primeTimeArray.size(),true);
          if(primeTimeArray.size()!=0)
          {
              Reporter.log("Prime Time button found. Please click",true);
              primeTimeArray.get(0).click();
              Reporter.log("Find the Prime Time text ID:"+primeTimeMsgID,true);
              WebElement primeTimeMsg =driver.findElement(By.id(primeTimeMsgID));
              Reporter.log("Surge message:"+primeTimeMsg.getText(),true);
              surge = extractPrimeAmount(primeTimeMsg.getText());
              Reporter.log("Surge:"+surge,true);
              surge = 100+extractPrimeAmount(primeTimeMsg.getText());
              Reporter.log("Click on the got it buttong id:"+gotItButtonID,true);
              WebElement gotItButton = driver.findElement(By.id(gotItButtonID));
              Reporter.log("Click got it button",true);
              gotItButton.click();
              //SendMail.email("Surge on Lyft", "Please report what are the values of UberX", "stathism@gmail.com,nikola.despotoski@gmail.com,sriravindran7@gmail.com,nihilvex@gmail.com");
          }
          Reporter.log("Get all Happy Hour buttons ID:"+HappyHourID,true);
          List<WebElement> happyHourArray = driver.findElements(By.id(HappyHourID));
          Reporter.log("Number of Happy Hour buttons:"+happyHourArray.size(),true);
          if(happyHourArray.size()!=0)
          {
              Reporter.log("Happy Hour button found",true);
              // SendMail.email("Happy Hour", "Please report what are the values of UberX", "stathism@gmail.com");
              //TODO send email to say that there is a price rise
          }
      }
      Reporter.log("Report the data",true);
      String response = ReportData.send("lyft",eta,""+surge,lat,lon);
      if(response==null || !response.equals("0"))
      {
          log.error("Was not able to recard the data");
          if(response==null)
              Assert.fail("response is null");
          else
              Assert.fail("response is equalt to:"+response);
      }
  }
  private void launchApp() throws MalformedURLException
  {
    //Appium setup
      Reporter.log("Setting up Appium",true);
	  String current = System.getProperty("user.dir");
	  Reporter.log("Curent direcotry:"+current,true);
      File appDir = new File(current+File.separator+"apk");
      File app = new File(appDir, apkFileName);  
      DesiredCapabilities capabilities2 = new DesiredCapabilities();
      capabilities2.setCapability("device", "Android");
      capabilities2.setCapability(CapabilityType.BROWSER_NAME, "");
      capabilities2.setCapability(CapabilityType.VERSION, "4.2");
      capabilities2.setCapability(CapabilityType.PLATFORM, "Mac");
      capabilities2.setCapability("platformName", "Android");
      capabilities2.setCapability("deviceName", "Nexus 4");
      capabilities2.setCapability("app", app.getAbsolutePath());
      capabilities2.setCapability("app-package", "me.lyft.android");
      capabilities2.setCapability("app-activity", "me.lyft.android.ui.StarterActivity");
      driver = new AppiumDriver(new URL("http://127.0.0.1:"+port+"/wd/hub"), capabilities2);
      Reporter.log("Appium setup finished",true);
  }
  private void login()
  {
      Reporter.log("Instantiate a WebDriverWait named wait",true);
      WebDriverWait wait = new WebDriverWait(driver,3);
      try
      {
          Reporter.log("Is there a login button there. ID:"+loginID,true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id(loginID)));
          Reporter.log("Instantiate the FB button",true);
          WebElement login =driver.findElement(By.id(loginID));
          Reporter.log("Click on the FB login button",true);
          login.click();
      }
      catch(TimeoutException e)
      {
          Reporter.log("No FB login button found",true);
      }
  }
  private void setAddress(String myAddress) throws InterruptedException
  {
      WebDriverWait wait = new WebDriverWait(driver,3);
      Reporter.log("Is there a Request Lyft button Now? ID:"+requestLyftID,true);
      wait.until(ExpectedConditions.presenceOfElementLocated(By.id(requestLyftID)));
      Reporter.log("Instantiate address field with ID:"+addressMainActivityID,true);
      WebElement addressMainActivity =driver.findElement(By.id(addressMainActivityID));
      Reporter.log("Click on the address field",true);
      addressMainActivity.click();
      Reporter.log("Set wait to 5 secs",true);
      wait = new WebDriverWait(driver,5);
      Reporter.log("Is there a back button there. ID:"+querryBackIconID,true);
      wait.until(ExpectedConditions.presenceOfElementLocated(By.id(querryBackIconID)));
      if(driver.findElements(By.id(clearID)).size()!=0)
      {
          Reporter.log("Instantiate clear button with ID:"+clearID,true);
          WebElement clearButton =driver.findElement(By.id(clearID));
          Reporter.log("Log in the clear button",true);
          clearButton.clear();
      }
      
      Reporter.log("Instantiate a querry field",true);
      WebElement querryField =driver.findElement(By.id(querryFieldID));
      Reporter.log("Enter the following address:"+myAddress,true);
      querryField.sendKeys(myAddress);
      Reporter.log("Send an ENTER command keycode=66",true);;
      driver.sendKeyEvent(66);
      Reporter.log("Set wait to 20 secs",true);
      wait = new WebDriverWait(driver,20);
      try
      {
          Reporter.log("Is there an entry with a little ballon. ID:"+ballonQuerryResultID,true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ballonQuerryResultID)));
      }
      catch(TimeoutException e)
      {
          Reporter.log("Send an ENTER command keycode=66",true);;
          driver.sendKeyEvent(66);
          Reporter.log("Is there an entry with a little ballon. ID:"+ballonQuerryResultID,true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ballonQuerryResultID)));
      }
      
      Reporter.log("Instantiate element with ID:"+querryResultsID,true);
      WebElement querryResults =driver.findElement(By.id(querryResultsID));

      Reporter.log("Get all the elemetns with ID:"+querryAddress,true);
      List<WebElement> addressArray = querryResults.findElements(By.id(querryAddress));
      Reporter.log("Number of elements found:"+addressArray.size(),true);
      int repeat =0;
      while(addressArray.size() == 0 && repeat<5 )
      {
          Thread.sleep(1000);
          Reporter.log("Re-Instantiate element with ID:"+querryResultsID,true);
          querryResults =driver.findElement(By.id(querryResultsID));
          Reporter.log("Re-Get all the elemetns with ID:"+querryAddress,true);
          addressArray = querryResults.findElements(By.id(querryAddress));
          Reporter.log("Number of elements found:"+addressArray.size(),true);
          repeat++;
      }
          
      for(WebElement address : addressArray)
      {
          Reporter.log("Address text:"+address.getText(),true);
          if(address.getText().contains(myAddress))
          {
              Reporter.log("Click on: "+ address.getText(),true);
              address.click();
              break;
          }
      }
      Reporter.log("Set wait to 20 secs",true);
      wait = new WebDriverWait(driver,20);
      Reporter.log("Is there a Request Lyft button Now? ID:"+requestLyftID,true);
      wait.until(ExpectedConditions.presenceOfElementLocated(By.id(requestLyftID)));
      Reporter.log("Sleep for max 5sec till address is found",true);
      timeoutAddress(5);
  }
  private String extractETA(String msg)
  {
      boolean minuteFound = false;
      int cars = -1;
      String[] words = msg.split("\\s+");
      for(String word : words)
      {
          if(NumberUtils.isNumber(word))
              cars = Integer.parseInt(word);
          
          if(word.toLowerCase().contains("minute"))
              minuteFound = true;
      }
      if(minuteFound)
          return ""+cars;
      else if(cars != -1)
      {
          Reporter.log("Only cars nearby were given... number of cars:"+cars,true);
          int estimateTime = Math.max(1,8-cars);
          return ""+estimateTime;
      }
      else
          return "-1";
          
  }
  private int extractPrimeAmount(String msg)
  {
      String[] words = msg.split("\\s+");
      for(String word : words)
      {
          if(word.endsWith("%"))
          {
              log.debug("word:"+word);
              if(word.startsWith("-"))
                  return  Integer.parseInt(word.substring(0, word.length()-1));
              else
                  return  Integer.parseInt(word.substring(1, word.length()-1));
          }
          if(word.endsWith("x"))
          {
              log.debug("word:"+word);
              if(word.startsWith("-"))
                  return  Integer.parseInt(word.substring(0, word.length()-1));
              else
              {
                  double temp = Double.parseDouble(word.substring(1, word.length()-1));
                  temp = temp*100 - 100;
                  log.debug("After convertion:"+temp);
                  return (int) temp ;
              }
          }
              
      }
      return 0;
  }
  private void timeoutAddress(int secs) throws InterruptedException
  {
      int loop = 0;
      int maxLoop = secs * 2;
      Reporter.log("Instantiate address field with ID:"+addressMainActivityID,true);
      WebElement addressMainActivity =driver.findElement(By.id(addressMainActivityID));
      while(addressMainActivity.getText().contains("Updating") && loop<maxLoop)
      {           
          Thread.sleep(500);
          addressMainActivity =driver.findElement(By.id(addressMainActivityID));
          loop +=1;
      }
      if(loop == maxLoop)
          new TimeoutException("Addess not found");
  }
  private void waitForMainPage()
  {
      Reporter.log("Set wait to 10 secs",true);
      WebDriverWait wait = new WebDriverWait(driver,10);
      try
      {
          Reporter.log("Is there a Request Lyft button there. ID:"+requestLyftID,true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id(requestLyftID)));
      }
      catch(TimeoutException e)
      {
          Reporter.log("Set wait to 3 secs",true);
          wait = new WebDriverWait(driver,3);
          Reporter.log("Is there a X button there. ID:"+cancelPopUpID,true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id(cancelPopUpID)));
          Reporter.log("Instantiate cancel object",true);
          WebElement cancelPopUp =driver.findElement(By.id(cancelPopUpID));
          Reporter.log("Get a point object our of the cancelPopUp image",true);
          final Point cancelPopUpPoint = cancelPopUp.getLocation();
          Reporter.log("Click on location x:"+cancelPopUpPoint.getX()+" y:"+cancelPopUpPoint.getY(),true);
          driver.executeScript("mobile: tap", new HashMap<String, Double>() 
                  {{ put("tapCount", 1.0); put("touchCount", 1.0); 
                  put("duration", 0.5); put("x", Double.parseDouble(""+cancelPopUpPoint.getX()));
                  put("y", Double.parseDouble(""+cancelPopUpPoint.getY())); }});
      }
  }
}

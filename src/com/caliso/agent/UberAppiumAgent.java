package com.caliso.agent;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.TestRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.caliso.utils.ManageVar;
import com.caliso.utils.ReportData;
import com.caliso.utils.ScreenshotListener;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;


public class UberAppiumAgent {
    
    private static AppiumDriver driver;
    private static boolean previousFailed = false;
    private static String email = "michael@caliso.bg";
    private static String password = "ys666Z";
    private static boolean firstTime = true;
    
    //Variables
    static private String port = "4724";
    static private double uberxX = 228.0;
    static private double uberxY = 1119;
    static private double uberSUVX = 710.0;
    static private double uberSUVY = 1119.0;
    
    @DataProvider(name = "DP1")
    public Object[][] createData() {
                String [][] standardAddresses = new String [][]{
                        {
                          "2127 24th Street San Francisco",
                          "37.753342", 
                          "-122.401074"
                        },
                        {
                          "480 Geary Street San Francisco",
                          "37.787428", 
                          "-122.411130"
                        },
                        {
                          "2794 California Street San Francisco",
                          "37.788308",
                          "-122.440283"
                        },
                        {
                          "301 Van Ness Avenue San Francisco",
                          "37.778678", 
                          "-122.420370"
                        },
                        {
                          "383 Garces Drive San Francisco",
                          "37.716778", 
                          "-122.483182"
                        },
                        {
                          "911 Paris Street San Francisco",
                          "37.715697", 
                          "-122.439576"
                        },
                        {
                          "250 Teddy Avenue San Francisco",
                          "37.715348", 
                          "-122.407476"
                        },
                        {
                          "1001 Portola Drive San Francisco",
                          "37.742126", 
                          "-122.455563"
                        },
                        {
                          "2299 Sunset Boulevard San Francisco",
                          "37.744349", 
                          "-122.494507"
                        },
                        {
                          "910 Church Street San Francisco",
                          "37.756487", 
                          "-122.428014"
                        },
                        {
                          "74 Carmel Street San Francisco",
                          "37.761274", 
                          "-122.448437"
                        },
                        {
                          "3601 Balboa Street San Francisco",
                          "37.775698", 
                          "-122.497556"
                        },
                        {
                          "1019 Clement Street San Francisco",
                          "37.784568", 
                          "-122.469987"
                        },
                        {
                          "1599 Lane Street San Francisco",
                          "37.732529", 
                          "-122.389488"
                        },
                        {
                          "1635 35th Avenue San Francisco",
                          "37.756765", 
                          "-122.494111"
                        },
                        {
                            "San Francisco International Airport",
                            "37.621559",
                            "-122.378950"
                        }
                       };
        return(standardAddresses);
    }

  
  @BeforeClass
  public void setup(ITestContext c) throws Exception
  {
      try
      {
          //Get variables
          port = ManageVar.getVarString("canoeport", port);
          uberxX = ManageVar.getVardouble("uberxX", uberxX);
          uberxY = ManageVar.getVardouble("uberxY", uberxY);
          uberSUVX = ManageVar.getVardouble("uberSUVX", uberSUVX);
          uberSUVY = ManageVar.getVardouble("uberSUVY", uberSUVY);
          
          //Appium setup
          launchApp();
          login();
          TestRunner tr = (TestRunner) c;
          tr.addTestListener(new ScreenshotListener(driver));
      }
      catch (Exception e)
      {
          Reporter.log("Error: "+ e.getMessage(),true);
          driver.quit();
          throw e;
      }
  }
  
  @AfterClass
  public void tearDown()
  {
      Reporter.log("Press the Home Button",true);
      driver.sendKeyEvent(3);
      driver.quit();
  }
  @BeforeMethod
  public void setupBeforeTest() throws MalformedURLException
  {
      if(previousFailed)
      {
          launchApp();
          login();
      }
      
      previousFailed = true;
  }
  String pinButtonId = "com.ubercab:id/ub__trip_view_pin_button";
  String addressIconId = "com.ubercab:id/ub__address_icon";
  String listOfPlaces = "com.ubercab:id/ub__locationsearch_viewgroup_content";
  String addressTextId = "com.ubercab:id/ub__address_text_address";
  String addressHeaderXPath = "//android.widget.TextView[contains(@text,'Addresses')]"; //This is used to make sure that we address was found
  String etaTextViewId ="com.ubercab:id/ub__trip_textview_eta_number";
  String addressListId = "com.ubercab:id/ub__locationsearch_listview_locations";
  String requestTripButtonId = "com.ubercab:id/ub__trip_button_request";
  
  WebElement places;
  WebElement pin;
  WebElement eta;
  
  String apkFileName="Uber-com.ubercab-30616-v3.0.20.apk";
  
  @Test(dataProvider = "DP1")
  public void getEtaAndPrice(String myAddress,String lat, String lon) throws InterruptedException, IOException 
  {
      String etaValue="";
      Double surge = 100.0;
      String previous = "";
      boolean foundinlist = false;

      Reporter.log("Instantiating a WebDriverWait object ",true);
      WebDriverWait wait = new WebDriverWait(driver, 30);
//      Takes too loong :-(      
//      Reporter.log("Waiting 30 sec till "+ pinButtonId);     
//      wait.until(ExpectedConditions.presenceOfElementLocated(By.id(pinButtonId)));
//      I know not good but otherwise there is a lot of time wastd...
      if(firstTime)
      {
          Reporter.log("Sleep for 5secs",true);
          Thread.sleep(5000);
      }
      else
      {
          Reporter.log("Sleep for 2secs",true);
          Thread.sleep(2000);
      }  
      
      if(places==null)
      {
          Reporter.log("Locating and click the the magnifying glass icon:"+ addressIconId,true);
          places =driver.findElement(By.id(addressIconId));
      }
      Reporter.log("Click on the places button",true);
      places.click();

      Reporter.log("Wainting 20 sec till the list of places is appearing:"+ listOfPlaces,true);
      wait.until(ExpectedConditions.presenceOfElementLocated(By.id(listOfPlaces)));
      
      foundinlist = clickOnAddress(myAddress);
      
      if(!foundinlist)
      {
          Assert.fail("Address not found");
      }
      if(firstTime)
      {
          Thread.sleep(1000); // We do this because it could take time to transition
          clickUberXInSanFrancisco();
          firstTime =false;
      }
      
      try{
          if(eta==null)
          {
              Reporter.log("Get ETA element",true);
              eta =driver.findElement(By.id(etaTextViewId));
          }
          Reporter.log("Get the text from the ETA WebElement",true);
          etaValue = eta.getText();
          Reporter.log("The ETA is of:"+etaValue,true);
      }
      catch(NoSuchElementException e)
      {
          Reporter.log("Could not find an ETA element. All cars must be busy",true);
          etaValue ="-1";
      }
      if(!etaValue.equals("-1"))
      {
          if(pin==null)
          {
              Reporter.log("Instantiate the pin button:"+pinButtonId,true);
              pin =driver.findElement(By.id(pinButtonId));
          }
          Reporter.log("Click on the location pin",true);
          pin.click();
          
          Reporter.log("Verify if the request trip button is present:"+requestTripButtonId,true);
          try
          {
              WebElement requestTripButton = driver.findElement(By.id(requestTripButtonId));
          }
          catch(NoSuchElementException e)
          {
              surge = 0.0;
              foundinlist=false;
              int repeat =0;
              while(!foundinlist && repeat<5 )
              {
                  Reporter.log("Get all the elements with classname:android.view.View",true);
                  List<WebElement> allElemetns = driver.findElements(By.className("android.view.View"));
                  
                  Reporter.log("Number of elements found:"+allElemetns.size(),true);
                  for(WebElement element:allElemetns)
                  {
                      String contentDesc =  element.getAttribute("name").toLowerCase().toLowerCase();
                      Reporter.log("content-desc:"+contentDesc,true);
                      if(contentDesc.endsWith("x"))
                      {
                          Reporter.log(contentDesc+" end with an x",true);
                          surge = Double.parseDouble(contentDesc.substring(0,contentDesc.length()-1));
                          Reporter.log("surge:"+surge,true);
                          surge = surge*100;
                          Reporter.log("submited surge:"+surge,true);
                          foundinlist = true;
                      }
                  }
                  repeat++;
              }
              if(!foundinlist)
              {
                  Assert.fail("Surge element not found!");
              }
          }
          Reporter.log("Click on back",true);
          clickBack();
      }
      Reporter.log("Report the data",true);
      String response = ReportData.send("uberx",etaValue,""+surge,lat,lon);
      if(response==null || !response.equals("0"))
      {
          Reporter.log("Was not able to recard the data",true);
          if(response==null)
              Assert.fail("response is null");
          else
              Assert.fail("response is equalt to:"+response);
      }
      Reporter.log("End Test",true);
      previousFailed = false;
      
  }
  private boolean clickOnAddress(String myAddress)
  {
      boolean foundinlist = false;
      String previous="";
      WebDriverWait wait = new WebDriverWait(driver, 30);
      int iteration = 0;
      
      Reporter.log("Instantiate the address Text:"+ addressTextId,true);
      WebElement addressField =driver.findElement(By.id(addressTextId));
      Reporter.log("Send key:"+myAddress,true);
      addressField.sendKeys(myAddress);
      while(!foundinlist && iteration < 3)
      {
          Reporter.log("Iteration # "+iteration,true);
          
          Reporter.log("Send an ENTER command keycode=66",true);
          driver.sendKeyEvent(66);
          Reporter.log("Locate the Address header on the resutls list. xpath:"+addressHeaderXPath,true);
          try{
              wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(addressHeaderXPath)));
              
              Reporter.log("Instantiate a webelement :"+addressListId,true);
              WebElement addressList =driver.findElement(By.id(addressListId));
              Reporter.log("Get all the elements of type android.widget.TextView in the list",true);
              List<WebElement> addressArray = addressList.findElements(By.className("android.widget.TextView"));
              
              
              for(WebElement address : addressArray)
              {
                  Reporter.log("TextView text:"+previous+" "+address.getText(),true);
                  if((previous+" "+address.getText()).contains(myAddress))
                  {
                      Reporter.log("Click on: "+ address.getText(),true);
                      address.click();
                      foundinlist = true;
                      break;
                  }
                  previous = address.getText();
              }
          }
          catch(TimeoutException e)
          {
              Reporter.log("Could not find the address section at all",true);
          } 
          iteration++;
      }
      
      return foundinlist;
  }
  private void clickUberXInSanFrancisco() throws InterruptedException
  {
      Reporter.log("Make sure UberX is not selected",true);
      HashMap<String, Double> command = new HashMap<String, Double>() {{ put("tapCount", 1.0); put("touchCount", 1.0);put("duration", 1.0); put("x", uberSUVX); put("y", uberSUVY); }};
      driver.executeScript("mobile: tap", command);
      Thread.sleep(250);
      Reporter.log("Make sure UberX is selected on the UberX",true);
      command = new HashMap<String, Double>() {{ put("tapCount", 1.0); put("touchCount", 1.0);put("duration", 1.0); put("x", uberxX); put("y", uberxY); }};
      driver.executeScript("mobile: tap", command);     
      Reporter.log("Script ended",true);
  }
  private void clickBack()
  {
      Reporter.log("Send an BACK command keycode=4",true);
      driver.sendKeyEvent(4);
      
  }
  private void launchApp() throws MalformedURLException
  {
    //Appium setup
      Reporter.log("Setting up Appium",true);
      String current = System.getProperty("user.dir");
      Reporter.log("Curent direcotry:"+current,true);
      File appDir = new File(current+File.separator+"apk");
      File app = new File(appDir, apkFileName);  
      DesiredCapabilities capabilities2 = new DesiredCapabilities();
      capabilities2.setCapability("device", "Android");
      capabilities2.setCapability(CapabilityType.BROWSER_NAME, "");
      capabilities2.setCapability(CapabilityType.VERSION, "4.2");
      capabilities2.setCapability(CapabilityType.PLATFORM, "Mac");
      capabilities2.setCapability("app", app.getAbsolutePath());
      capabilities2.setCapability("app-package", "com.ubercab");
      capabilities2.setCapability("app-activity", ".UBUberActivity");
      capabilities2.setCapability("platformName", "Android");
      capabilities2.setCapability("deviceName", "Nexus 4");
      driver = new AppiumDriver(new URL("http://127.0.0.1:"+port+"/wd/hub"), capabilities2);
      Reporter.log("Appium setup finished",true);
  }
  
  private void login()
  {
      
      try{
          Reporter.log("Instantiating a WebDriverWait object",true);
          WebDriverWait wait = new WebDriverWait(driver, 3);
          Reporter.log("Waiting 3 sec till com.ubercab:id/ubc__magic_button_signin",true);
          wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.ubercab:id/ubc__magic_button_signin")));
          Reporter.log("Looking for a button with id=com.ubercab:id/ubc__magic_button_signin",true);
          WebElement loginButton = driver.findElement(By.id("com.ubercab:id/ubc__magic_button_signin"));
         
          Reporter.log("Click on the login button",true);
          loginButton.click();
          
          String emailFieldId = "com.ubercab:id/ub__signin_edittext_email";
          String passwordFieldId = "com.ubercab:id/ub__signin_edittext_password";
          String loginButtonId = "com.ubercab:id/ub__signin_button_signin";
          
          Reporter.log("Locate email field with id: " + emailFieldId,true);
          WebElement emailField = driver.findElement(By.id(emailFieldId));
          Reporter.log(String.format("Enter the %s to the email field",email),true);
          emailField.sendKeys(email);
          Reporter.log("Locate password field with id: " + passwordFieldId,true);
          WebElement passwordField = driver.findElement(By.id(passwordFieldId));
          Reporter.log(String.format("Enter the %s to the password field",password),true);
          passwordField.sendKeys(password);
          Reporter.log("Locate the login button with id: " + loginButtonId,true);
          WebElement signinButton = driver.findElement(By.id(loginButtonId));
          Reporter.log("Click on the login button",true);
          signinButton.click();  
      }
      catch(TimeoutException e)
      {
          Reporter.log("No login button found the user must alredy been loggedin",true);
      } 
  }
  
}

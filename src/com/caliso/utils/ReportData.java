package com.caliso.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;

import com.squareup.okhttp.OkHttpClient;

public class ReportData {
    
    public static String send(String service, String eta,String surge,String lat, String lon) throws IOException
    {
        InputStream in = null;
        String urlString = String.format(
                "http://api.ridecanoe.com/set_feed_coord.php?appid=1&lat=%s&lon=%s&service=%s&eta=%s&surge=%s",
                lat,lon,service,eta,surge);
        Reporter.log("Url:"+urlString,true);
        URL url = new URL(urlString);
        Reporter.log("Instantiate OkHttpClient",true);
        OkHttpClient client = new OkHttpClient();
        Reporter.log("Connect to "+urlString,true);
        HttpURLConnection connection = client.open(url);
        try {
          // Read the response.
          Reporter.log("Read the response.",true);
          in = connection.getInputStream();
          Reporter.log("Convert to Array",true);
          byte[] response = org.apache.commons.io.IOUtils.toByteArray(in);
          Reporter.log("Return: "+ new String(response, "UTF-8"),true);
          return new String(response, "UTF-8");
        } finally {
          if (in != null) in.close();
        }
    }
}

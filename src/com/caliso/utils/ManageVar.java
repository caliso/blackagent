package com.caliso.utils;

import org.testng.Reporter;

public class ManageVar {
    public static String getVarString(String var, String defaultVal)
    {
        String exPortval = System.getenv(var);
        Reporter.log("Env variable "+var+" is equal to:"+exPortval,true);
        if(exPortval!= null &&!exPortval.isEmpty())
            return exPortval;
        else
            return defaultVal;
    }
    public static double getVardouble(String var, double defaultVal)
    {
        String exPortval = System.getenv(var);
        Reporter.log("Env variable "+var+" is equal to:"+exPortval,true);
        if(exPortval!= null &&!exPortval.isEmpty())
            return Double.parseDouble(exPortval);
        else
            return defaultVal;
    }
    

}

package com.caliso.utils;

import io.appium.java_client.AppiumDriver;

import java.io.File;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

public class ScreenshotListener extends TestListenerAdapter {
    private AppiumDriver driver;
   
    public ScreenshotListener(AppiumDriver dr)
    {
        this.driver = dr;
    }
    @Override
    public void onTestFailure(ITestResult tr) 
    {
        try{
            File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            Reporter.log("The screenshoot was captured at:"+file.getAbsolutePath(),true);
            boolean result = file.renameTo(new File(System.getProperty("user.dir")+"/"+"screenshot.png"));
            Reporter.log("Was the screenshoot moved:"+result,true);
        }
        catch(Exception e)
        {Reporter.log("Error occured while taking the screenshoot:"+e.getMessage(),true);}
        driver.quit();
    }
  
  }

package com.caliso.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.testng.Reporter;

public class SendMail {
    
    
    static public void email(String title, String msg, String destination)
    {
       String cmd =  "echo \" "+ msg+"\" | mail -s \"" +title+"\" "+ destination;
       Reporter.log("Execute:"+cmd,true);
       String output = executeCommand(cmd);
       Reporter.log("Response:"+output,true);  
    }
    private static String executeCommand(String command) {
        
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            String[] cmd = {
                    "/bin/sh",
                    "-c",
                    command
                    };
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = 
                            new BufferedReader(new InputStreamReader(p.getInputStream()));
 
                        String line = "";           
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return output.toString();
 
    }
}
